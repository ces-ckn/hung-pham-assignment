<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin page</title>
<script type="text/javascript">
	function checkForm() {

		var userName = document.getElementById('username').value.trim();
		var password = document.getElementById('password').value.trim();
		var matchPassword = document.getElementById('matchPassword').value.trim();

		if (userName.length == 0 || password.length == 0
				|| matchPassword.length == 0) {

			document.getElementById('error').innerHTML = "Please input all fields";
			document.getElementById('error').style.display = "block";
			return false;
		}

		if (password != matchPassword) {
			document.getElementById('error').innerHTML = "Passwords don't match, try again!";
			document.getElementById('error').style.display = "block";
			return false;
		}
		
		if (password.length < 6) {
			document.getElementById('error').innerHTML = "Password must be at least 6 characters";
			document.getElementById('error').style.display = "block";
			return false;
		}
		
		document.getElementById('error').style.display = "none";
		return true;
	}
</script>
</head>

<body>
	<div style="display: block">
		<div style="float: left">
			 <a href="beer">Beers</a> | <a href="category">Categories</a> | Users 
		</div>
		<div style="float: right">
			<!-- For login user -->
			<c:url value="/j_spring_security_logout" var="logoutUrl" />
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
			<script>
				function formSubmit() {
					document.getElementById("logoutForm").submit();
				}
			</script>

			<c:if test="${pageContext.request.userPrincipal.name != null}">
		
			User : ${pageContext.request.userPrincipal.name} | <a
					href="javascript:formSubmit()"> Logout</a>

			</c:if>
		</div>
	</div>

	<br />

	<div style="display: block">

		<h2>Add New Administrator</h2>
		<div style="color: green"><c:out value="${param['successMsg']}"></c:out></div>
		<div style="color: red"><c:out value="${param['errorMsg']}"></c:out></div>
		<label id="error" style="display: none; color: red"></label>
		<c:url var="addAction" value="/adduser"></c:url>
		<form:form method="POST" action="${addAction}"
			onsubmit="return checkForm()">			
			<table>
				<tr>
					<td><form:label path="username">User Name</form:label></td>
					<td><form:input path="username" /></td>
				</tr>
				<tr>
					<td><form:label path="password">Password</form:label></td>
					<td><form:input path="password" type="password" /></td>
				</tr>
				<tr>
					<td><form:label path="matchPassword">Retype Password</form:label></td>
					<td><form:input path="matchPassword" type="password" /></td>
				</tr>
				<tr>
					<td><input type="submit" value="Submit" /></td>
					<td></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>