<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>
<title>Beer Page</title>
<style type="text/css">
.tg {
	border-collapse: collapse;
	border-spacing: 0;
	border-color: #ccc;
}

.tg td {
	font-family: Arial, sans-serif;
	font-size: 14px;
	padding: 10px 5px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: #ccc;
	color: #333;
	background-color: #fff;
}

.tg th {
	font-family: Arial, sans-serif;
	font-size: 14px;
	font-weight: normal;
	padding: 10px 5px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: #ccc;
	color: #333;
	background-color: #f0f0f0;
}

.tg .tg-4eph {
	background-color: #f9f9f9
}
</style>

<script type="text/javascript">
	function checkForm() {

		var beerName = document.getElementById('name').value.trim();		

		if (beerName.length == 0) {
			document.getElementById('error').innerHTML = "Please enter beer name!";
			document.getElementById('error').style.display = "block";
			return false;
		}
		document.getElementById('error').style.display = "none";
		return true;
	}
</script>

</head>
<body>
	<div style="display: block">
		<div style="float: left">
			Beers | <a href="category">Categories</a> | <a href="admin">Users</a>
		</div>
		<div style="float: right">
			<!-- For login user -->
			<c:url value="/j_spring_security_logout" var="logoutUrl" />
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
			<script>
				function formSubmit() {
					document.getElementById("logoutForm").submit();
				}
			</script>

			<c:if test="${pageContext.request.userPrincipal.name != null}">
		
			User : ${pageContext.request.userPrincipal.name} | <a
					href="javascript:formSubmit()"> Logout</a>

			</c:if>
		</div>
	</div>

	<br />

	<div style="display: block">

		<h2>Add a Beer</h2>
		<label id="error" style="display: none; color: red"></label>
		<c:url var="addAction" value="/beer/add"></c:url>

		<form:form action="${addAction}" commandName="beer" onsubmit="return checkForm()">
			<table>
				<c:if test="${!empty beer.name}">
					<tr>
						<td><form:label path="id">
								<spring:message text="ID" />
							</form:label></td>
						<td><form:input path="id" readonly="true" size="8"
								disabled="true" /> <form:hidden path="id" /></td>
					</tr>
				</c:if>
				<tr>
					<td><form:label path="categoryId">
							<spring:message text="Choose Category" />
						</form:label></td>
					<td><select style="width: 172px" name="categoryId">
							<c:forEach items="${listCategories}" var="category">
								<option value="${category.id}">
									<c:out value="${category.name}"></c:out>
								</option>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td><form:label path="name">
							<spring:message text="Name" />
						</form:label></td>
					<td><form:input path="name" /></td>
				</tr>
				<tr>
					<td><form:label path="description">
							<spring:message text="Description" />
						</form:label></td>
					<td><form:input path="description" /></td>
				</tr>
				<tr>
					<td><form:label path="manufacturer">
							<spring:message text="Manufacturer" />
						</form:label></td>
					<td><form:input path="manufacturer" /></td>
				</tr>
				<tr>
					<td><form:label path="country">
							<spring:message text="Country" />
						</form:label></td>
					<td><form:input path="country" /></td>
				</tr>
				<tr>
					<td><form:label path="price">
							<spring:message text="Price" />
						</form:label></td>
					<td><form:input path="price" /></td>
				</tr>
				<tr>
					<td><form:label path="archived">
							<spring:message text="Archived" />
						</form:label></td>
					<td><form:checkbox path="archived" /></td>
				</tr>
				<tr>
					<td colspan="2"><c:if test="${!empty beer.name}">
							<input type="submit" value="<spring:message text="Edit Beer"/>" />
						</c:if> <c:if test="${empty beer.name}">
							<input type="submit" value="<spring:message text="Add Beer"/>" />
						</c:if></td>
				</tr>
			</table>
		</form:form>
		<br>

		<h3>Beer List</h3>
		<c:if test="${!empty listBeers}">
			<table class="tg">
				<tr>
					<th width="80">ID</th>
					<th width="80">Category ID</th>
					<th width="120">Name</th>
					<th width="120">Description</th>
					<th width="120">Manufacturer</th>
					<th width="120">Country</th>
					<th width="120">Price</th>
					<th width="120">Archived</th>
					<th width="120">Actions</th>
				</tr>
				<c:forEach items="${listBeers}" var="beer">
					<tr>
						<td>${beer.id}</td>
						<td>${beer.categoryId}</td>
						<td>${beer.name}</td>
						<td>${beer.description}</td>
						<td>${beer.manufacturer}</td>
						<td>${beer.country}</td>
						<td>${beer.price}</td>
						<c:choose>
							<c:when test="${beer.archived == false}">
								<td><c:out value="No"></c:out></td>
							</c:when>
							<c:otherwise>
								<td><c:out value="Yes"></c:out></td>
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${beer.archived == false}">
								<td><a
									href="<c:url value='/editBeer/${beer.categoryId}/${beer.id}' />">Edit</a>
									| <a href="<c:url value='/removeBeer/${beer.id}' />">Delete</a>
									| <a href="<c:url value='/archiveBeer/${beer.id}' />">Archive</a>
								</td>
							</c:when>
							<c:otherwise>
								<td><a
									href="<c:url value='/editBeer/${beer.categoryId}/${beer.id}' />">Edit</a>
									| <a href="<c:url value='/removeBeer/${beer.id}' />">Delete</a>
									| <a href="<c:url value='/unarchiveBeer/${beer.id}' />">Unarchive</a>
								</td>
							</c:otherwise>

						</c:choose>
					</tr>
				</c:forEach>
			</table>
		</c:if>
		<br>

	</div>
</body>
</html>