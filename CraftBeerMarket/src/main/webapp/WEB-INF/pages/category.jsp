<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>
<title>Category Page</title>
<style type="text/css">
.tg {
	border-collapse: collapse;
	border-spacing: 0;
	border-color: #ccc;
}

.tg td {
	font-family: Arial, sans-serif;
	font-size: 14px;
	padding: 10px 5px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: #ccc;
	color: #333;
	background-color: #fff;
}

.tg th {
	font-family: Arial, sans-serif;
	font-size: 14px;
	font-weight: normal;
	padding: 10px 5px;
	border-style: solid;
	border-width: 1px;
	overflow: hidden;
	word-break: normal;
	border-color: #ccc;
	color: #333;
	background-color: #f0f0f0;
}

.tg .tg-4eph {
	background-color: #f9f9f9
}
</style>
</head>
<body>
	<div style="display: block">
		<div style="float: left">
			<a href="beer">Beers</a> | Categories | <a href="admin">Users</a> 
		</div>
		<div style="float: right">
			<!-- For login user -->
			<c:url value="/j_spring_security_logout" var="logoutUrl" />
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>
			<script>
				function formSubmit() {
					document.getElementById("logoutForm").submit();
				}
			</script>

			<c:if test="${pageContext.request.userPrincipal.name != null}">
		
			User : ${pageContext.request.userPrincipal.name} | <a
					href="javascript:formSubmit()"> Logout</a>

			</c:if>
		</div>
	</div>

	<br />

	<div style="display: block">

		<h2>Add a Category</h2>
		<div style="color: red"><c:out value="${param['existCagetoryNameMsg']}"></c:out></div>
		<c:url var="addAction" value="/category/add"></c:url>

		<form:form action="${addAction}" commandName="category">
			<table>
				<c:if test="${!empty category.name}">
					<tr>
						<td><form:label path="id">
								<spring:message text="ID" />
							</form:label></td>
						<td><form:input path="id" readonly="true" size="8"
								disabled="true" /> <form:hidden path="id" /></td>
					</tr>
				</c:if>
				<tr>
					<td><form:label path="name">
							<spring:message text="Name" />
						</form:label></td>
					<td><form:input path="name" /></td>
				</tr>
				<tr>
					<td><form:label path="">
							<spring:message text="Description" />
						</form:label></td>
					<td><form:input path="description" /></td>
				</tr>
				<tr>
					<td colspan="2"><c:if test="${!empty category.name}">
							<input type="submit"
								value="<spring:message text="Edit Category"/>" />
						</c:if> <c:if test="${empty category.name}">
							<input type="submit"
								value="<spring:message text="Add Category"/>" />
						</c:if></td>
				</tr>
			</table>
		</form:form>
		<br>
		<h3>Categories List</h3>
		<div style="color: red"><c:out value="${param['errorDeleteCategoryMsg']}"></c:out></div>
		
		<c:if test="${!empty listCategories}">
			<table class="tg">
				<tr>
					<th width="80">Category ID</th>
					<th width="120">Category Name</th>
					<th width="120">Category Description</th>
					<th colspan="2" width="120">Actions</th>

				</tr>
				<c:forEach items="${listCategories}" var="category">
					<tr>
						<td>${category.id}</td>
						<td>${category.name}</td>
						<td>${category.description}</td>
						<td colspan="2"><a
							href="<c:url value='/category/edit/${category.id}' />">Edit</a> | <a
							href="<c:url value='/category/remove/${category.id}' />">Delete</a></td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
	</div>
</body>
</html>