package com.ces.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ces.dto.AdminUserRegisterDto;
import com.ces.service.UserService;

@Controller
public class LoginController {
	
	@Autowired
    private UserService service;

    private static final String LOGOUT_SUCCESFUL_MESSAGE = "You've been logged out successfully.";
    private static final String INVALID_USER_ACCOUNT = "Invalid username or password!";

    @RequestMapping(value={ "/", "/login" }, method=RequestMethod.GET)
    public ModelAndView showLoginPage(@RequestParam(value="error", required=false) String error,
            @RequestParam(value="logout", required=false) String logout,
            HttpServletRequest request) {

        ModelAndView view = new ModelAndView();

        if (error != null) {
            view.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
        }

        if (logout != null) {
            view.addObject("msg", getErrorMessage(request, LOGOUT_SUCCESFUL_MESSAGE));
        }
        view.setViewName("login");
        return view;
    }

    // for 403 access denied page
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accesssDenied() {

        ModelAndView model = new ModelAndView();

        // check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails) auth.getPrincipal();
            System.out.println(userDetail);

            model.addObject("username", userDetail.getUsername());

        }

        model.setViewName("403");
        return model;

    }

    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
    public ModelAndView adminPage() {

        ModelAndView model = new ModelAndView("admin", "command", new AdminUserRegisterDto());       
        return model;

    }

    @RequestMapping(value = "/adduser", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") AdminUserRegisterDto user, ModelMap model) {
        if (service.addUser(user)){
        	model.addAttribute("successMsg", "Create admin " + user.getUsername() + " successfully!");
        } else {
        	model.addAttribute("errorMsg", "The username " + user.getUsername() + " is already in use. Please enter a different username!");
        }
    	
        return "redirect:/admin";
    }

    
    // customize the error message
    private String getErrorMessage(HttpServletRequest request, String key) {

        Exception exception = (Exception) request.getSession().getAttribute(key);

        String error = INVALID_USER_ACCOUNT;

        if (exception instanceof BadCredentialsException || exception instanceof AuthenticationServiceException) {
            error = INVALID_USER_ACCOUNT;
        } else if (key.equals(LOGOUT_SUCCESFUL_MESSAGE)) {
            error = LOGOUT_SUCCESFUL_MESSAGE;
        } else if (exception instanceof LockedException) {
            error = exception.getMessage();
        }

        return error;
    }
}
