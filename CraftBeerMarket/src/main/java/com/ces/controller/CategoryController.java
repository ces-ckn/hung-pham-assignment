package com.ces.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.ces.model.Category;
import com.ces.service.CategoryService;
import com.ces.model.Beer;
import com.ces.service.BeerService;

/**
 * This controller routes accesses to the application to the appropriate hanlder
 * methods.
 * 
 * @author Hung (Charles) V. PHAM
 *
 */

@Controller
public class CategoryController {
	private CategoryService categoryService;
	private BeerService beerService;
	
	private static final String EXIST_CATEGORY_MESSAGE = "This category name is already in use. Please enter a different category name!";
	private static final String ERROR_DELETE_CATEGORY_MESSAGE = "Cannot delete this category because there are some beers that are using it!";

	@Autowired(required = true)
	@Qualifier(value = "categoryService")
	public void setCategoryService(CategoryService cs) {
		this.categoryService = cs;
	}

	@Autowired(required = true)
	@Qualifier(value = "beerService")
	public void setBeerService(BeerService bs) {
		this.beerService = bs;
	}

	@RequestMapping(value = "/category", method = RequestMethod.GET)
	public String listCategories(Model model) {
		model.addAttribute("category", new Category());
		model.addAttribute("listCategories", this.categoryService.listCategory());
		return "category";
	}

	// Category Zone
	// For add and update category both
	@RequestMapping(value = "/category/add", method = RequestMethod.POST)
	public String addCategory(@ModelAttribute("category") Category category, Model model) {

		if (category.getId() == 0) {
			// new category, add it, check exist name of category
			if (this.categoryService.addCategory(category)) {
				model.addAttribute("existCagetoryNameMsg", EXIST_CATEGORY_MESSAGE);
			} 
		} else {
			// existing category, call update
			this.categoryService.updateCategory(category);
		}

		return "redirect:/category";

	}

	@RequestMapping("/category/remove/{id}")
	public String removeCategory(@PathVariable("id") int id, Model model) {
		if (this.beerService.listBeerByCatelogy(id).size() != 0) {
			model.addAttribute("errorDeleteCategoryMsg", ERROR_DELETE_CATEGORY_MESSAGE);
		} else {
			this.categoryService.removeCategory(id);
		}
		return "redirect:/category";
	}

	@RequestMapping("/category/edit/{id}")
	public String editCategory(@PathVariable("id") int id, Model model) {
		model.addAttribute("category", this.categoryService.getCategoryById(id));
		model.addAttribute("listCategories", this.categoryService.listCategory());
		return "category";
	}

	// Beer Zone
	@RequestMapping(value = "/beer", method = RequestMethod.GET)
	public String listBeers(Model model) {
		model.addAttribute("beer", new Beer());
		model.addAttribute("listCategories", this.categoryService.listCategory());
		model.addAttribute("listBeers", this.beerService.listBeerNotArc());
		return "beer";
	}

	// For add and update beer both
	@RequestMapping(value = "/beer/add", method = RequestMethod.POST)
	public String addBeer(@ModelAttribute("beer") Beer beer, HttpServletRequest request) {

		int category_id = Integer.valueOf(request.getParameter("categoryId"));
		Boolean archived = Boolean.parseBoolean(request.getParameter("archived"));

		beer.setCategoryId(category_id);
		beer.setArchived(archived);

		if (beer.getId() == 0) {
			// new beer, add it
			this.beerService.addBeer(beer);
		} else {
			// existing beer, call update
			this.beerService.updateBeer(beer);
		}

		return "redirect:/beer";
	}

	// remove Beer
	@RequestMapping("/removeBeer/{id}")
	public String removeBeer(@PathVariable("id") int id) {
		this.beerService.removeBeer(id);
		return "redirect:/beer";
	}

	// Edit Beer
	@RequestMapping("/editBeer/{category_id}/{id}")
	public String editBeer(@PathVariable("category_id") int category_id, @PathVariable("id") int id, Model model) {
		model.addAttribute("beer", this.beerService.getBeerById(id));
		model.addAttribute("listCategories", this.categoryService.listCategory());
		model.addAttribute("listBeers", this.beerService.listBeer());
		return "beer";
	}

	// archive Beer
	@RequestMapping("/archiveBeer/{id}")
	public String archiveBeer(@PathVariable("id") int id) {
		this.beerService.archiveBeer(id, true);
		return "redirect:/beer";
	}

	// unarchive Beer
	@RequestMapping("/unarchiveBeer/{id}")
	public String unarchiveBeer(@PathVariable("id") int id) {
		this.beerService.archiveBeer(id, false);
		return "redirect:/beer";
	}

}
