package com.ces.dao;

import org.hibernate.Session;

import com.ces.model.AccessToken;

public interface AccessTokenDao {
	
	public boolean addAccessToken(AccessToken accessToken, Session session);
	public String getUsernameFromAccessToken(String accessToken);

}
