package com.ces.dao;

import java.util.Set;

import com.ces.model.Users;
import com.ces.common.HibernateUtils;
import com.ces.model.UserInfo;
import com.ces.model.UserRoles;

import org.hibernate.Session;

public class UserDaoImpl implements UserDao{
	@Override
    public Users findByUserName(String username, Session session) {
        Users user = null;

        if (session != null && session.isOpen()) {
            user = (Users) session.createQuery("from Users where username=?")
                    .setParameter(0, username).uniqueResult();
        }

        return user;
    }
	
	@Override
    public boolean addUser(Users user, Session session) {

        if (user != null) {
            session.save(user);
            session.flush();

            return true;
        }

        return false;
    }
	
	@Override
    public boolean addUserRole(Set<UserRoles> userRole, Session session) {

        if (userRole.size() > 0 && HibernateUtils.isValidSession(session)) {
            for (UserRoles userRoles : userRole) {
                session.save(userRoles);
                session.flush();
            }

            return true;
        }

        return false;
    }

	@Override
	public boolean addUserInfo(UserInfo userInfo, Session session) {
		if (userInfo != null) {
            session.save(userInfo);
            session.flush();
            return true;
        }

        return false;
	}

}
