package com.ces.dao;

import java.util.List;
import java.util.Map;

import com.ces.model.Beer;

public interface BeerDao {
	
	List<Beer> getBeerByCatelogy(int catelogy);

    List<Beer> getAllBeerLst();
    
    List<Beer> getAllBeerLstNotArc();
    
    public Beer getBeerById(int id);
    
    public void addBeer(Beer beer);

    public void updateBeer(Beer beer);

    public void removeBeer(int id);
    
    public List<Map<String, Object>> retrieveBeer(String username);

}
