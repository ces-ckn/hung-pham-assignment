package com.ces.dao;

import com.ces.model.Passport;

public interface PassportDao {
	public void addPassport(Passport passport);
	public boolean isExist(String username, int beer_id);
}
