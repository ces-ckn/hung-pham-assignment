package com.ces.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ces.model.Beer;
import com.ces.model.Passport;
import com.ces.common.HibernateUtils;
import com.ces.dao.CategoryDaoImpl;

public class BeerDaoImpl implements BeerDao {

	private static final Logger logger = LoggerFactory.getLogger(CategoryDaoImpl.class);	

	@SuppressWarnings("unchecked")
	@Override
	public List<Beer> getBeerByCatelogy(int catelogy) {
		Session session = HibernateUtils.getSession();
		
		return (List<Beer>) session
				.createQuery("from Beer where category_id=? and archived = false").setParameter(0, catelogy).list();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Beer> getAllBeerLst() {
		Session session = HibernateUtils.getSession();
		return (List<Beer>) session.createQuery("from Beer where archived = false")
				.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Beer> getAllBeerLstNotArc() {
		Session session = HibernateUtils.getSession();
		return (List<Beer>) session.createQuery("from Beer  order by id asc").list();
	}

	@Override
	public Beer getBeerById(int id) {
		Session session = HibernateUtils.getSession();
		Beer beer = (Beer) session.createQuery("from Beer where id=?").setParameter(0, id).uniqueResult();
		logger.info("Beer loaded successfully, Beer details=" + beer);
		return beer;				
	}

	@Override
	public void addBeer(Beer beer) {
		Session session = HibernateUtils.getSession();
		session.persist(beer);
		logger.info("Beer saved successfully, Beer Details=" + beer);
	}

	@Override
	public void updateBeer(Beer beer) {
		Session session = HibernateUtils.getSession();
		session.update(beer);
		logger.info("Beer updated successfully, Beer Details=" + beer);
	}

	@Override
	public void removeBeer(int id) {
		Session session = HibernateUtils.getSession();
		Beer beer = (Beer) session.load(Beer.class, new Integer(id));
		Query query = session.createQuery("delete from Passport where beer_id = :id");
		query.setParameter("id", id);
		
		int result = query.executeUpdate();
		
		if (result >= 0){
			if (null != beer) {
				session.delete(beer);
			}
		}	
		
		logger.info("Beer deleted successfully, Beer details=" + beer);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> retrieveBeer(String username) {
		Session session = HibernateUtils.openSession();
		
		List<Beer> listBeer= (List<Beer>) session.createQuery("from Beer").list();
		
		List<Passport> listPassport = session.createQuery("from Passport where username=?").setParameter(0, username).list();

		List<Map<String, Object>> objResponse = new ArrayList<Map<String, Object>>();
		
		int beersSize = listBeer.size();
		int passportsSize = listPassport.size();
		
		for (int i = 0; i < beersSize; i++){
			Beer beer = listBeer.get(i);
			HashMap<String, Object> objChild = new HashMap<String, Object>();
			objChild.put("id", beer.getId());			
			objChild.put("name", beer.getName());
			objChild.put("manufacturer", beer.getManufacturer());
			objChild.put("country", beer.getCountry());
			objChild.put("price", beer.getPrice());
			objChild.put("description", beer.getDescription());
			objChild.put("archived", beer.getArchived());
			boolean consumed = false;
			for (int j = 0; j < passportsSize; j++){
				if (beer.getId() == listPassport.get(j).getBeer().getId()){
					consumed = true;
					break;
				}
			}			
			if (consumed){
				objChild.put("consumed", "yes");
			} else{
				objChild.put("consumed", "no");
			}
			objResponse.add(objChild);	
		}
		
		return objResponse;				
	}	
}
