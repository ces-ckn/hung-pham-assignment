package com.ces.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ces.common.HibernateUtils;
import com.ces.model.Category;

/**
 * An implementation of the CategoryDao interface.
 * @author Hung (Charles) V. PHAM
 *
 */

@Repository
public class CategoryDaoImpl implements CategoryDao{
		
private static final Logger logger = LoggerFactory.getLogger(CategoryDaoImpl.class);
            
    @Override
    public Boolean addCategory(Category category) {
    	 if(this.isExistName(category.getName())){
             return true;
         }else{
             Session session = HibernateUtils.getSession();
             session.persist(category);
             logger.info("Category saved successfully, Category Details = " + category);
             return false;
         }       
    }

    @Override
    public void updateCategory(Category category) {
        Session session = HibernateUtils.getSession();
        session.update(category);
        logger.info("Category updated successfully, Category Details= " + category);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Category> listCategory() {
        Session session = HibernateUtils.getSession();
        List<Category> categoryList = session.createQuery("from Category").list();
        for(Category category : categoryList){
            logger.info("Category List::"+category);
        }
        return categoryList;
    }

    @Override
    public Category getCategoryById(int id) {
        Session session = HibernateUtils.getSession();
        Category category = (Category) session.load(Category.class, new Integer(id));
        logger.info("Category loaded successfully, Category details="+category);
        return category;
    }

    @Override
    public void removeCategory(int id) {
        Session session = HibernateUtils.getSession();
        Category category = (Category) session.load(Category.class, new Integer(id));
        if(null != category){
            session.delete(category);
        }
        logger.info("Category deleted successfully, Category details="+category);
    }
    
    public Boolean isExistName(String name){
        Session session = HibernateUtils.getSession();
        Query query=  session.createQuery("from Category where name= :name");
        query.setParameter("name", name);
        Category category = (Category) query.uniqueResult();
        if(category!=null){
           return true;
        }else{
          return false;
        } 
    }

}
