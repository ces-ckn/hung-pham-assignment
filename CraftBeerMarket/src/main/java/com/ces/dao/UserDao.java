package com.ces.dao;

/**
 * Created by Hung (Charles) V. PHAM on 09/22/2015.
 */

import java.util.Set;

import org.hibernate.Session;

import com.ces.model.Users;
import com.ces.model.UserInfo;
import com.ces.model.UserRoles;

public interface UserDao {
	public Users findByUserName(String username, Session session);
	public boolean addUser(Users user, Session session);
    public boolean addUserRole(Set<UserRoles> userRole, Session session);
    public boolean addUserInfo(UserInfo userInfo, Session session);
}
