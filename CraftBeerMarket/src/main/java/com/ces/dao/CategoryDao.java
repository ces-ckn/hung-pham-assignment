package com.ces.dao;

import java.util.List;

import com.ces.model.Category;

/**
 * Defines DAO operations for the category model.
 * @author Hung (Charles) V. PHAM
 *
 */


public interface CategoryDao {
	
	public Boolean addCategory(Category category);

    public void updateCategory(Category category);

    public List<Category> listCategory();

    public Category getCategoryById(int id);

    public void removeCategory(int id);

}
