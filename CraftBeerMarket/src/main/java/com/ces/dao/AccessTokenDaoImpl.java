package com.ces.dao;

import org.hibernate.Session;

import com.ces.common.HibernateUtils;
import com.ces.model.AccessToken;
import com.ces.model.Users;

public class AccessTokenDaoImpl implements AccessTokenDao{

	@SuppressWarnings("unchecked")
	@Override
	public boolean addAccessToken(AccessToken accessToken, Session session) {
		if (accessToken != null) {
            session.save(accessToken);
            session.flush();
            return true;
        }
        return false;		
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getUsernameFromAccessToken(String token) {
		Session session = HibernateUtils.openSession();
		AccessToken accessToken = (AccessToken) session.createQuery("from AccessToken where token = ?").setParameter(0, token).uniqueResult();
		if (accessToken == null){
			return null;
		}
		return accessToken.getUsers().getUsername();
	}

}
