package com.ces.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ces.common.HibernateUtils;
import com.ces.model.Passport;

public class PassportDaoImpl implements PassportDao {
	private static final Logger logger = LoggerFactory.getLogger(PassportDaoImpl.class);

	@Override
	public void addPassport(Passport passport) {
		logger.info("==================PassportDaoImpl.addPassport=============================" + passport);
		Session session = HibernateUtils.openSession();
		session.save(passport);
		session.flush();
		logger.info("Passport saved successfully, Passport details=" + passport);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isExist(String username, int beer_id) {
		Session session = HibernateUtils.openSession();
		Query query = session.createQuery("from Passport where username=? and beer_id=?");
		List<Passport> listPassport =  query.setParameter(0, username).setParameter(1, beer_id).list();
		if (listPassport.size() == 0){
			return false;
		}
			
		return true;
	}

}
