package com.ces.service;

public interface PassportService {
	public void addPassport(String username, int beer_id);
	public boolean isExist(String username, int beer_id);
}
