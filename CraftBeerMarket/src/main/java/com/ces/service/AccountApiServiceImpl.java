package com.ces.service;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.math.BigInteger;
import java.security.SecureRandom;

import org.hibernate.Session;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ces.common.HibernateUtils;
import com.ces.common.Utils;
import com.ces.dao.AccessTokenDao;
import com.ces.dao.UserDao;
import com.ces.model.AccessToken;
import com.ces.model.UserInfo;
import com.ces.model.UserRoles;
import com.ces.model.Users;
import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

public class AccountApiServiceImpl implements AccountApiService {
	private static final Logger logger = LoggerFactory
			.getLogger(AccountApiServiceImpl.class);

	@Autowired
	private UserDao userDao;

	@Autowired
	private AccessTokenDao accessTokenDao;

	/**
	 * @return the userDao
	 */
	public UserDao getUserDao() {
		return userDao;
	}

	/**
	 * @param userDao
	 *            the userDao to set
	 */
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	/**
	 * @return the accessTokenDao
	 */
	public AccessTokenDao getAccessTokenDao() {
		return accessTokenDao;
	}

	/**
	 * @param accessTokenDao
	 *            the accessTokenDao to set
	 */
	public void setAccessTokenDao(AccessTokenDao accessTokenDao) {
		this.accessTokenDao = accessTokenDao;
	}

	@Override
	public JSONObject signUp(String paramsAccount) {
		logger.info("Receiver:******************** " + paramsAccount);
		
		Session session = HibernateUtils.openSession();
		JSONObject objResponse = new JSONObject();
		try {
			BasicDBObject accountObj = (BasicDBObject) JSON
					.parse(paramsAccount);
			Users users = new Users();
			Set<UserRoles> role = new HashSet<UserRoles>();

			UserRoles userRole = new UserRoles();
			userRole.setRole("ROLE_USER");
			userRole.setUsers(users);
			role.add(userRole);
			String username = accountObj.getString("username");
			String password = accountObj.getString("password");
			
			users.setUsername(username);
			users.setPassword(Utils.encryptPass(password));
			users.setEnabled(true);
			users.setUserRoleses(role);

			UserInfo userInfo = new UserInfo(users);
			userInfo.setUsername(username);
			userInfo.setAddress(accountObj.getString("address"));
			userInfo.setEmail(accountObj.getString("email"));
			userInfo.setFirstName(accountObj.getString("first_name"));
			userInfo.setLastName(accountObj.getString("last_name"));

			AccessToken accessToken = new AccessToken();
			accessToken.setUsers(users);			
			String token = generateAccessToken(username, password);
			accessToken.setToken(token);
			accessToken.setExpiresTime(new Date());
			
			users.setUserInfo(userInfo);

			if (userDao.addUser(users, session)) {				
				if (userDao.addUserRole(role, session)) {
					userDao.addUserInfo(userInfo, session);
					accessTokenDao.addAccessToken(accessToken, session);
				}
			}

			JSONObject childObject = new JSONObject();
			childObject.put("username", username);			
			childObject.put("email", accountObj.getString("email"));
			childObject.put("first_name", accountObj.getString("first_name"));
			childObject.put("last_name", accountObj.getString("last_name"));
			childObject.put("address", accountObj.getString("address"));
			
			objResponse.put("accessToken", token);			
			objResponse.put("user", childObject);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return objResponse;

	}

	@Override
	public String getUsernameFromAccessToken(String accessToken) {
		return accessTokenDao.getUsernameFromAccessToken(accessToken);
	}

	private String generateAccessToken(String username, String password) {
		SecureRandom secureRandom = new SecureRandom();
		String random = new BigInteger(130, secureRandom).toString(32);

		String keySource = username + password + random;
		return Utils.encryptPass(keySource);
	}

}
