package com.ces.service;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import com.ces.common.HibernateUtils;
import com.ces.common.Utils;
import com.ces.dao.UserDao;
import com.ces.dto.AdminUserRegisterDto;
import com.ces.model.AccessToken;
import com.ces.model.UserInfo;
import com.ces.model.UserRoles;
import com.ces.model.Users;

public class UserServiceImpl implements UserService{

	@Autowired
    private UserDao userDao;

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
   
    @Override
    public boolean addUser(AdminUserRegisterDto user) {
        Session session = HibernateUtils.openSession();
        if (userDao.findByUserName(user.getUsername(), session) != null){
        	return false;        	
        }        
        Users users = new Users();
        Set<UserRoles> role = new HashSet<UserRoles>();
        UserRoles userRole = new UserRoles();
        userRole.setRole("ROLE_ADMIN");
        userRole.setUsers(users);
        role.add(userRole);
        users.setUsername(user.getUsername());
        users.setPassword(Utils.encryptPass(user.getPassword()));
        users.setEnabled(true);
        users.setUserRoleses(role);

        if (userDao.addUser(users, session)) {

            return userDao.addUserRole(role, session);
        }

        return false;
    }

	@Override
	public boolean signUp(Users users, UserInfo userInfo, AccessToken accessToken) {
		// TODO Auto-generated method stub
		return false;
	}
}
