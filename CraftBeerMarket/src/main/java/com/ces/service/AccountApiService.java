package com.ces.service;


import org.json.simple.JSONObject;

public interface AccountApiService {
	
	/**
     * Sign up.
     *
     * @param paramsAccount the params account
     * @return the jSON object
     */
    public JSONObject signUp(String paramsAccount);

    
    /**
     * Gets user id from access token.
     *
     * @param accessToken the access token
     * @return the username from access token
     */
    public String getUsernameFromAccessToken(String accessToken);


}
