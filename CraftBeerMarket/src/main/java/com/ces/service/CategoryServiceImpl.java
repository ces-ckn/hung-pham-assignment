package com.ces.service;

import java.util.List;

import com.ces.dao.CategoryDao;
import com.ces.model.Category;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryServiceImpl implements CategoryService{
	
	private CategoryDao categoryDao;

    public void setCategoryDao(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    @Transactional
    public Boolean addCategory(Category category) {
        return this.categoryDao.addCategory(category);
    }

    @Override
    @Transactional
    public void updateCategory(Category category) {
        this.categoryDao.updateCategory(category);
    }

    @Override
    @Transactional
    public List<Category> listCategory() {
        return this.categoryDao.listCategory();
    }

    @Override
    @Transactional
    public Category getCategoryById(int id) {
        return this.categoryDao.getCategoryById(id);
    }

    @Override
    @Transactional
    public void removeCategory(int id) {
        this.categoryDao.removeCategory(id);
    }
}
