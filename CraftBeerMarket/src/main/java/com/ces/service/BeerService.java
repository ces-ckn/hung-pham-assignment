package com.ces.service;

import java.util.List;
import java.util.Map;

import com.ces.model.Beer;

public interface BeerService {

public List<Beer> listBeer();
    
    public List<Beer> listBeerNotArc();
    
    public List<Beer> listBeerByCatelogy(int catelogy);
    
    public Beer getBeerById(int id);
    
    public void addBeer(Beer beer);

    public void updateBeer(Beer beer);
    
    public void removeBeer(int id);
    
    public void archiveBeer(int id, boolean archived);
    
    public List<Map<String, Object>>  retrieveBeer(String username);
}
