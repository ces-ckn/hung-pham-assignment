package com.ces.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ces.dao.BeerDao;
import com.ces.dao.PassportDao;
import com.ces.dao.PassportDaoImpl;
import com.ces.dao.UserDao;
import com.ces.model.Passport;
import com.ces.common.HibernateUtils;

public class PassportServiceImpl implements PassportService {
	private static final Logger logger = LoggerFactory.getLogger(PassportServiceImpl.class);

    @Autowired
    private PassportDao passportDao;
    
    @Autowired
    private UserDao userDao;
    
    @Autowired
    private BeerDao beerDao;

	/**
	 * @return the passportDao
	 */
	public PassportDao getPassportDao() {
		return passportDao;
	}

	/**
	 * @param passportDao the passportDao to set
	 */
	public void setPassportDao(PassportDao passportDao) {
		this.passportDao = passportDao;
	}

	/**
	 * @return the userDao
	 */
	public UserDao getUserDao() {
		return userDao;
	}

	/**
	 * @param userDao the userDao to set
	 */
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	/**
	 * @return the beerDao
	 */
	public BeerDao getBeerDao() {
		return beerDao;
	}

	/**
	 * @param beerDao the beerDao to set
	 */
	public void setBeerDao(BeerDao beerDao) {
		this.beerDao = beerDao;
	}

	@Override
    @Transactional
    public void addPassport(String username, int beer_id) {
		logger.info("==================PassportServiceImpl.addPassport=============================");
		Passport passport = new Passport();
        passport.setUsers(this.getUserDao().findByUserName(username, HibernateUtils.openSession()));
        passport.setBeer(this.getBeerDao().getBeerById(beer_id));
        this.getPassportDao().addPassport(passport);
    }

	@Override
	public boolean isExist(String username, int beer_id) {
		return this.getPassportDao().isExist(username, beer_id);
	}    

}
