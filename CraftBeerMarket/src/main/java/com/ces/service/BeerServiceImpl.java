package com.ces.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ces.service.BeerService;
import com.ces.dao.BeerDao;
import com.ces.model.Beer;


public class BeerServiceImpl implements BeerService{
	@Autowired
    private BeerDao beerDao;
    
    @Override
    @Transactional
    public List<Beer> listBeer() {
        return getBeerDao().getAllBeerLst();
    }
    
    @Override
    @Transactional
    public List<Beer> listBeerByCatelogy(int catelogy) {
        return getBeerDao().getBeerByCatelogy(catelogy);
    }
    
    @Override
    @Transactional
    public List<Beer> listBeerNotArc() {
        return getBeerDao().getAllBeerLstNotArc();
    }
    
    @Override
    @Transactional
    public void addBeer(Beer beer) {
        this.beerDao.addBeer(beer);
    }

    @Override
    @Transactional
    public void updateBeer(Beer beer) {
       this.beerDao.updateBeer(beer);
    }

    @Override
    @Transactional
    public void removeBeer(int id) {
        this.beerDao.removeBeer(id);
    }
    @Override
    @Transactional
    public Beer getBeerById(int id) {
        return this.beerDao.getBeerById(id);
    }
    /**
     * @return the beerDao
     */
    public BeerDao getBeerDao() {
        return beerDao;
    }

    /**
     * @param beerDao the beerDao to set
     */
    public void setBeerDao(BeerDao beerDao) {
        this.beerDao = beerDao;
    }

	@Override
	@Transactional
	public void archiveBeer(int id, boolean archived) {		
		Beer beer = this.getBeerById(id);
		beer.setArchived(archived);
		this.updateBeer(beer);
	}

	@Override
	public List<Map<String, Object>>  retrieveBeer(String username) {
		return this.beerDao.retrieveBeer(username);
	}
}
