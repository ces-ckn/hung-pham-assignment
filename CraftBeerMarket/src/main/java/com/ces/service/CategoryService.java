package com.ces.service;

import java.util.List;

import com.ces.model.Category;

public interface CategoryService {

	public Boolean addCategory(Category category);

	public void updateCategory(Category category);

	public List<Category> listCategory();

	public Category getCategoryById(int id);

	public void removeCategory(int id);
}
