package com.ces.service;

import com.ces.dto.AdminUserRegisterDto;
import com.ces.model.AccessToken;
import com.ces.model.UserInfo;
import com.ces.model.Users;

public interface UserService {
	public boolean addUser(AdminUserRegisterDto user);
	public boolean signUp(Users users, UserInfo userInfo, AccessToken accessToken);

}
