package com.ces.common;

public class ApiError {
	public static final int ERROR_CODE_USERNAME_EXISTING = 1001;
	public static final String ERROR_MSG_USERNAME_EXISTING = "This username is already in use. Please enter a different username!";
	
	public static final int ERROR_CODE_BEER_NOT_EXISTING = 1002;
	public static final String ERROR_MSG_BEER_NOT_EXISTING = "This beer is not existing!";
	
	public static final int ERROR_CODE_ACCESS_TOKEN_INVALID = 1003;
	public static final String ERROR_MSG_ACCESS_TOKEN_INVALID = "Invalid access token!";
	
	public static final int ERROR_CODE_BEER_CONSUMED = 1004;
	public static final String ERROR_MSG_BEER_CONSUMED = "You consumed this beer already";
	
}