package com.ces.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ces.common.ApiError;
import com.ces.common.Utils;
import com.ces.controller.BaseCPController;
import com.ces.model.Beer;
import com.ces.model.Passport;
import com.ces.service.AccountApiService;
import com.ces.service.BeerService;
import com.ces.service.PassportService;
import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

@RestController
public class AccountApiController extends BaseCPController {
	private static final Logger logger = LoggerFactory.getLogger(AccountApiController.class);
	@Autowired
	private AccountApiService accountApiService;
	
	@Autowired
	private BeerService beerService;
	
	@Autowired
	private PassportService passportService;

	@RequestMapping(value = "/api/v1/customers/signup", method = RequestMethod.POST)
	public Map<String, Object> register(@RequestBody final String params, HttpServletRequest request) {
		logger.info("Receiver: " + params);
		BasicDBObject accountObj = (BasicDBObject) JSON.parse(params);
		String username = accountObj.getString("username");
		if (Utils.isExists(username)){		
			Map<String, Object> errorObject = new HashMap<String, Object>();
			errorObject.put("code", ApiError.ERROR_CODE_USERNAME_EXISTING);
			errorObject.put("message", ApiError.ERROR_MSG_USERNAME_EXISTING);
			return resultMap(false, errorObject);	
		}
		JSONObject objResponse = accountApiService.signUp(params);
		return resultMap(true, objResponse);
	}
	
	
	@RequestMapping(value = "/api/v1/customers/addbeer", method = RequestMethod.POST)
	public Map<String, Object> addBeer(@RequestBody final String params, HttpServletRequest request){
		logger.info("==================addBeer========================" + params);
		BasicDBObject accountObj = (BasicDBObject) JSON.parse(params);
		String accessToken = accountObj.getString("access_token");
		int beerId = accountObj.getInt("beer_id");
		String username = accountApiService.getUsernameFromAccessToken(accessToken);
		if (username != null && !username.isEmpty()){
			Beer beer = this.beerService.getBeerById(beerId);
			if (beer == null){			
				Map<String, Object> errorObject = new HashMap<String, Object>();
				errorObject.put("code", ApiError.ERROR_CODE_BEER_NOT_EXISTING);
				errorObject.put("message", ApiError.ERROR_MSG_BEER_NOT_EXISTING);
				return resultMap(false, errorObject);				
			}
			if (!passportService.isExist(username, beerId)){
				passportService.addPassport(username, beerId);
				JSONObject objResponse = new JSONObject();			
				objResponse.put("beer", beer);			
				return resultMap(true, objResponse);
			} else {
				Map<String, Object> errorObject = new HashMap<String, Object>();
				errorObject.put("code", ApiError.ERROR_CODE_BEER_CONSUMED);
				errorObject.put("message", ApiError.ERROR_MSG_BEER_CONSUMED);
				return resultMap(false, errorObject);		
			}
		} else {
			Map<String, Object> errorObject = new HashMap<String, Object>();
			errorObject.put("code", ApiError.ERROR_CODE_ACCESS_TOKEN_INVALID);
			errorObject.put("message", ApiError.ERROR_MSG_ACCESS_TOKEN_INVALID);
			return resultMap(false, errorObject);						
		}		
	}	
	
	@RequestMapping(value = "/api/v1/customers/retrievebeer", method = RequestMethod.POST)
	public Map<String, Object> retrieveBeer(@RequestBody final String token, HttpServletRequest request){
		logger.info("==================retrieveBeer========================" + token);
		BasicDBObject accountObj = (BasicDBObject) JSON.parse(token);
		String accessToken = accountObj.getString("access_token");
		String username = accountApiService.getUsernameFromAccessToken(accessToken);
		if (username != null && !username.isEmpty()){
			List<Map<String, Object>> objResponse = beerService.retrieveBeer(username);	
			return resultMap(true, objResponse); 
		}else {
			Map<String, Object> errorObject = new HashMap<String, Object>();
			errorObject.put("code", ApiError.ERROR_CODE_ACCESS_TOKEN_INVALID);
			errorObject.put("message", ApiError.ERROR_MSG_ACCESS_TOKEN_INVALID);
			return resultMap(false, errorObject);						
		}			
	}
	
}
