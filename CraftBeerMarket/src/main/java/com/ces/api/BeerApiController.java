package com.ces.api;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ces.model.Beer;
import com.ces.service.BeerService;
import com.ces.controller.BaseCPController;

@RestController
public class BeerApiController extends BaseCPController{
	@Autowired
    private BeerService beerService;

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/api/v1/beers/listByCatelogy/{catelogy}", method = RequestMethod.GET)
    public Map<String, Object> getBeerByCatelogy(@PathVariable int catelogy) {

        List<Beer> beerLst = beerService.listBeerByCatelogy(catelogy);

        JSONArray jsonArray = new JSONArray();
        jsonArray.addAll(beerLst);

        return resultMap(true, jsonArray);
    }


    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/api/v1/beers/list", method = RequestMethod.GET)
    public Map<String, Object> getAllBeerLst() {

        List<Beer> beerLst = beerService.listBeer();

        JSONArray jsonArray = new JSONArray();
        jsonArray.addAll(beerLst);

        return resultMap(true, jsonArray);
    }

}
